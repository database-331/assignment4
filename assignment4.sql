CREATE OR REPLACE PACKAGE book_store AS
    
    FUNCTION get_price_after_tax( book_isbn NUMBER)
    RETURN NUMBER;
    PROCEDURE show_purchases;
    PROCEDURE rename_category(book_category VARCHAR2,new_category VARCHAR2);

END book_store;

/

CREATE OR REPLACE PACKAGE BODY book_store AS

TYPE custIds IS
VARRAY(10000000) OF NUMBER;

        afterDiscount NUMBER;
        originalCost NUMBER;
        discountGiven NUMBER;
        afterTax Number;
        
          
    FUNCTION price_after_discount(book_isbn NUMBER)
    RETURN NUMBER
    AS
    originalCost books.cost%type;
    discountGiven books.discount%type;
    afterDiscount NUMBER;
    
    BEGIN
    
       SELECT
        b.retail INTO originalCost
        FROM
        Books b
        WHERE
        book_isbn=b.ISBN;
        

        SELECT
        NVL(b.discount,0) INTO discountGiven
        FROM
        Books b
        WHERE
        book_isbn=b.ISBN;
        
        afterDiscount:=originalCost-discountGiven;
       
        RETURN afterDiscount;
    END;
    
    FUNCTION get_price_after_tax(book_isbn NUMBER)
    RETURN NUMBER
    AS
    afterTax NUMBER;
    afterDiscount NUMBER;
    
        BEGIN
        afterDiscount:=book_store.price_after_discount(book_isbn);
            afterTax:= afterDiscount*1.15;
            RETURN afterTax;
            END;
--assignment 3         
    FUNCTION book_purchasers(book_isbn NUMBER)
    RETURN custIds
    AS
    customers custIds;
    
    BEGIN
    SELECT DISTINCT
        O.customer# BULK COLLECT INTO customers
    FROM
    Books B INNER JOIN OrderItems OI
    USING(ISBN)
    INNER JOIN ORDERS O
    USING(order#)
    WHERE
    ISBN=book_isbn;
    
    RETURN customers;
    END;
    
    PROCEDURE show_purchases
    AS
    customerIDs custIDs;
    a_row Books%ROWTYPE;
    custFirst Customers.FirstName%TYPE;
    custLast Customers.LastName%TYPE;
    BEGIN
    customerIDs := custIDs();
    FOR a_row IN (
                    SELECT DISTINCT
                        Title, ISBN
                    FROM
                        Books)
        LOOP
        
        dbms_output.put_line(a_row.isbn || ': ' || a_row.title || ', ');
            customerIDs:=book_purchasers(a_row.isbn);
        For i in 1 .. customerIDs.COUNT
            LOOP
                SELECT DISTINCT
                    firstName, lastName INTO custFirst, custLast
                FROM
                    Customers
                WHERE
                    customer#=customerIDs(i);
                    
            dbms_output.put_line(custFirst || ' ' || custLast);
            END LOOP;
            END LOOP;
        END;

--assignment 4
    PROCEDURE rename_category(book_category VARCHAR2,new_category VARCHAR2)
    AS
    book_category book.category%type;
    new_category VARCHAR2;
    BEGIN
    
    UPDATE books
    SET category=new_category
    WHERE category='book_category';
--    FOR a_row in (
--        SELECT
--            *
--        FROM
--            books
--        WHERE
--        category=book_category)
        
    END;
                
    
 
    
        
END book_store;

/

--DECLARE
--book1 NUMBER;
--book2 NUMBER;
--b1Final NUMBER;
--b2Final NUMBER;
--
--BEGIN
--    SELECT
--    ISBN INTO book1
--    FROM
--    BOOKS
--    WHERE
--    title ='BUILDING A CAR WITH TOOTHPICKS';
--
--    SELECT
--    ISBN INTO book2
--    FROM
--    BOOKS
--    WHERE
--    title ='HOLY GRAIL OF ORACLE';
--    
--    b1Final:=book_store.get_price_after_tax(book1);
--    b2Final:=book_store.get_price_after_tax(book2);
--    dbms_output.put_line('BUILDING A CAR WITH TOOTHPICKS ' || b1Final);
--    dbms_output.put_line('HOLY GRAIL OF ORACLE ' || b2Final);
--
--END;
/
--/
--SELECT
--        b.cost 
--        FROM
--        Books b
--        WHERE
--        1059831198=b.ISBN;
--/
--SELECT
--        NVL(b.discount,0) 
--        FROM
--        Books b
--        WHERE
--        1059831198=b.ISBN;

/
--assignment 3
--DECLARE
--
--purchasers book_store.custIDs;
--BEGIN
--purchasers := book_store.book_purchasers(3437212490);
--SELECT
--    firstName
--    FROM
--    Customers
--    WHERE 
--    customer#=purchasers(1);
--END;


--BEGIN
--    book_store.show_purchases();
--END;

--assignment 4
